package golib

import (
	"crypto/sha1"
	"encoding/hex"
	"errors"
	"fmt"
	"os"
)

func PanicWrapper(wrapped func()) (err error) {

	defer func() {

		if cause := recover(); cause != nil {
			err = errors.New(fmt.Sprintf("%v", cause))
		}
	}()
	wrapped()
	return err
}

func FileExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func Sha1(bytes []byte) string {
	h := sha1.New()
	h.Write(bytes)
	return hex.EncodeToString(h.Sum(nil))
}
