package must

import (
	"os"
	"path/filepath"

	"gitlab.com/mr_balloon/golib"
)

func BeNoErr(err error) {
	if err != nil {
		panic(err)
	}
}

func BeErr(err error) {
	if err == nil {
		panic(err)
	}
}

// BeAtModDir tries to changes pwd to root of project (go.mod file)
func BeAtModDir() {
	for i := 0; i < 10; i = i + 1 {
		path, _ := os.Getwd()
		exist, err := golib.FileExists(filepath.Join(path, "./go.mod"))
		if err != nil {
			panic(err)
		}
		if exist {
			return
		}
		err = os.Chdir("./..")
		if err != nil {
			panic(err)
		}
	}
	panic("Could not go to root dir")
}
