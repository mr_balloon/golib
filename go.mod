module gitlab.com/mr_balloon/golib

go 1.16

require (
	github.com/icza/dyno v0.0.0-20210726202311-f1bafe5d9996 // indirect
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/thoas/go-funk v0.9.0 // indirect
	github.com/tidwall/gjson v1.8.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.1.7 // indirect
)
