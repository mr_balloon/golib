package hmap

import "github.com/mitchellh/mapstructure"

// Fill takes ptr to struct and fills fields from
// field of map
func (h H) Fill(target interface{}) error {
	return mapstructure.Decode(h, target)
}
