package hmap

import (
	"github.com/thoas/go-funk"
)

// H is generic hashmap, method with i at last
// will do implict type conversion
type H map[string]interface{}

func (h H) Empty() bool {
	for range h {
		return false
	}
	return true
}

func (h H) Keys() []string {
	keys := make([]string, 0, len(h))
	for k := range h {
		keys = append(keys, k)
	}
	return keys
}

func (h H) Only(keys ...string) bool {
	if len(h) > len(keys) {
		return false
	}

	for hkey := range h {
		if !funk.ContainsString(keys, hkey) {
			return false
		}
	}
	return true
}

func (h H) Some(keys ...string) H {
	h2 := make(H, 0)
	for k, v := range h {
		if funk.ContainsString(keys, k) {
			h2[k] = v
		}
	}
	return h2
}

func (h H) Get(key string) interface{} {
	return h[key]
}
