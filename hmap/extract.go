package hmap

func (h H) Booli(key string, val ...bool) bool {
	def := false
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(bool)
	if ok {
		return vv
	}

	switch vvv := v.(type) {
	case string:
		if vvv == "true" {
			return true
		} else if vvv == "false" {
			return false
		}
		return def
	default:
		return def
	}
}

func (h H) Bytei(key string, val ...[]byte) []byte {
	var def []byte
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.([]byte)
	if ok {
		return vv
	}
	switch vvv := v.(type) {
	case string:
		return []byte(vvv)
	default:
		return def
	}
}
func (h H) Complex128i(key string, val ...complex128) complex128 {
	var def complex128
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(complex128)
	if ok {
		return vv
	}
	return def
}
func (h H) Complex64i(key string, val ...complex64) complex64 {
	var def complex64
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(complex64)
	if ok {
		return vv
	}
	return def
}

func (h H) Float64i(key string, val ...float64) float64 {
	var def float64
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(float64)
	if ok {
		return vv
	}
	return def
}

func (h H) Float32i(key string, val ...float32) float32 {
	var def float32
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(float32)
	if ok {
		return vv
	}
	return def
}

func (h H) Inti(key string, val ...int) int {
	var def int
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(int)
	if ok {
		return vv
	}
	return def
}

func (h H) Int64i(key string, val ...int64) int64 {
	var def int64
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(int64)
	if ok {
		return vv
	}
	return def
}

func (h H) Int32i(key string, val ...int32) int32 {
	var def int32
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(int32)
	if ok {
		return vv
	}
	return def
}

func (h H) Int16i(key string, val ...int16) int16 {
	var def int16
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(int16)
	if ok {
		return vv
	}
	return val[0]
}

func (h H) Int8i(key string, val ...int8) int8 {
	var def int8
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(int8)
	if ok {
		return vv
	}
	return def
}

func (h H) Stringi(key string, val ...string) string {
	var def string
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(string)
	if ok {
		return vv
	}
	return def
}

func (h H) Uinti(key string, val ...uint) uint {
	var def uint
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(uint)
	if ok {
		return vv
	}
	return def
}

func (h H) Uint64i(key string, val ...uint64) uint64 {
	var def uint64
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(uint64)
	if ok {
		return vv
	}
	return def
}

func (h H) Uint32i(key string, val ...uint32) uint32 {
	var def uint32
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(uint32)
	if ok {
		return vv
	}
	return def
}

func (h H) Uint16i(key string, val ...uint16) uint16 {
	var def uint16
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(uint16)
	if ok {
		return vv
	}
	return def
}
func (h H) Uint8i(key string, val ...uint8) uint8 {
	var def uint8
	if len(val) != 0 {
		def = val[0]
	}

	v, ok := h[key]
	if !ok {
		return def
	}
	vv, ok := v.(uint8)
	if ok {
		return vv
	}
	return def
}

// fix => me also implemnt other conversion for
// 			int or other implicit conversion

// switch vvv := v.(type) {
// case int:
// 	return val[0]
// case int8:

// case int16:

// case int32:

// case int64:
// case bool:
// case float32:
// case float64:
// case uint8:
// case uint16:
// case uint32:
// case uint64:
// case string:

// default:

// }
