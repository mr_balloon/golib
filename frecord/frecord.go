package frecord

import (
	"io"
	"io/ioutil"
	"net/http"
)

// F is a record that might represent file like object or
// data read from file or url to the file

type F struct {
	reader io.ReadCloser
	url    string
	bytes  []byte
}

func FromByte(b []byte) *F {
	return &F{
		bytes: b,
	}
}

func FromReader(r io.ReadCloser) *F {
	return &F{
		reader: r,
	}
}

func FromURL(url string) *F {
	return &F{
		url: url,
	}
}

func (f *F) CanStream() bool {
	return f.reader != nil
}

func (f *F) CanByte() bool {
	return f.reader != nil || f.bytes != nil
}

func (f *F) CanRedirrect() bool {
	return f.url != ""
}

func (f *F) GetStream() io.ReadCloser {
	return f.reader
}

func (f *F) GetBytes() []byte {
	if f.bytes == nil {
		bytes, err := ioutil.ReadAll(f.reader)
		if err != nil {
			// fixme => proper err handeling
			return nil
		}
		return bytes
	}

	return f.bytes
}

func (f *F) GetURL() string {
	return f.url
}

func (f *F) HTTPWrite(w http.ResponseWriter) {
	// fixme => if stream then do streaming write / multichunk binary ?
	// 			if url redirrect ro that url
	// 			if bytes wite with content type binary
}
