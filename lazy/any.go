package lazy

import (
	"encoding/json"
	"errors"

	"github.com/icza/dyno"
	"github.com/tidwall/gjson"
	"github.com/tidwall/sjson"
	"gitlab.com/mr_balloon/golib/hmap"
)

var (
	ErrNotFound = errors.New("not found")
)

type Map struct {
	bdata  []byte
	data   hmap.H
	isByte bool
}

func New(data interface{}) Map {
	switch ty := data.(type) {
	case []byte:
		return FromBytes(ty)
	case map[string]interface{}:
		return FromAny(ty)
	case map[string][]string:
		dh := make(hmap.H)
		for k, v := range ty {
			dh[k] = v
		}
		return FromAny(dh)

	case map[string]string:
		dh := make(hmap.H)
		for k, v := range ty {
			dh[k] = v
		}
		return FromAny(dh)
	default:
		return FromAny(hmap.H{
			"data": ty,
		})
	}
}

func FromAny(data hmap.H) Map {
	return Map{
		bdata:  nil,
		data:   data,
		isByte: false,
	}
}

func FromBytes(bytes []byte) Map {
	return Map{
		bdata:  bytes,
		data:   nil,
		isByte: true,
	}
}

func (m *Map) AsBytes() ([]byte, error) {
	if m.isByte {
		return m.bdata, nil
	}
	return json.Marshal(m.data)
}

func (m *Map) AsHMap() (hmap.H, error) {
	if !m.isByte {
		return m.data, nil
	}
	var h hmap.H
	err := json.Unmarshal(m.bdata, &h)
	return h, err
}

func (m *Map) AsStruct(i interface{}) error {
	if !m.isByte {
		return m.data.Fill(i)
	}
	return json.Unmarshal(m.bdata, i)
}

func (m *Map) Set(value interface{}, path ...string) error {
	if m.isByte {
		return m.setBytes(value, concat(path...))
	}
	return dyno.SSet(m.data, value, path...)
}

func (m *Map) Get(path ...string) (interface{}, error) {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, concat(path...))
		if r.Exists() {
			return nil, ErrNotFound
		}
		return r.Value(), nil
	}
	return dyno.SGet(m.data, path...)
}

func (m *Map) setBytes(value interface{}, path string) error {
	nb, err := sjson.SetBytes(m.bdata, path, value)
	if err != nil {
		return err
	}
	m.bdata = nb
	return nil
}

func (m *Map) Delete(path ...string) error {
	if m.isByte {
		return m.deleteBytes(concat(path...))
	}
	ipaths := make([]interface{}, 0, len(path))
	for i, p := range path {
		if i == 0 {
			continue
		}

		ipaths = append(ipaths, p)
	}
	return dyno.Delete(m.data, path[0], ipaths...)
}

func (m *Map) deleteBytes(path string) error {
	nb, err := sjson.DeleteBytes(m.bdata, path)
	if err != nil {
		return err
	}
	m.bdata = nb
	return nil
}

func (m *Map) Booli(key string, val ...bool) bool {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, key)
		if r.Exists() {
			return r.Bool()
		}
		if len(val) > 0 {
			return val[0]
		}
		return false
	}
	return m.data.Booli(key, val...)
}

func (m *Map) Floati(key string, val ...float64) float64 {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, key)
		if r.Exists() {
			return r.Float()
		}
		if len(val) > 0 {
			return val[0]
		}
		return 0
	}
	return m.data.Float64i(key, val...)
}
func (m *Map) Int64i(key string, val ...int64) int64 {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, key)
		if r.Exists() {
			return r.Int()
		}
		if len(val) > 0 {
			return val[0]
		}
		return 0
	}
	return m.data.Int64i(key, val...)
}

func (m *Map) Stringi(key string, val ...string) string {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, key)
		if r.Exists() {
			return r.String()
		}
		if len(val) > 0 {
			return val[0]
		}
		return ""
	}
	return m.data.Stringi(key, val...)

}
func (m *Map) Uint64i(key string, val ...uint64) uint64 {
	if m.isByte {
		r := gjson.GetBytes(m.bdata, key)
		if r.Exists() {
			return r.Uint()
		}
		if len(val) > 0 {
			return val[0]
		}
		return 0
	}
	return m.data.Uint64i(key, val...)
}
