package lazy

import (
	"strings"
)

func concat(ss ...string) string {
	if len(ss) == 1 {
		return ss[0]
	}

	var buf strings.Builder

	for _, s := range ss {
		buf.WriteString(s)
	}
	return buf.String()
}
