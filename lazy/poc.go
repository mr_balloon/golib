package lazy

/*

//github.com/icza/dyno

func Append(v interface{}, value interface{}, path ...interface{}) error
func AppendMore(v interface{}, values []interface{}, path ...interface{}) error
func ConvertMapI2MapS(v interface{}) interface{}
func Delete(v interface{}, key interface{}, path ...interface{}) error
func Get(v interface{}, path ...interface{}) (interface{}, error)
func GetBoolean(v interface{}, path ...interface{}) (bool, error)
func GetFloat64(v interface{}, path ...interface{}) (float64, error)
func GetFloating(v interface{}, path ...interface{}) (float64, error)
func GetInt(v interface{}, path ...interface{}) (int, error)
func GetInteger(v interface{}, path ...interface{}) (int64, error)
func GetMapI(v interface{}, path ...interface{}) (map[interface{}]interface{}, error)
func GetMapS(v interface{}, path ...interface{}) (map[string]interface{}, error)
func GetSlice(v interface{}, path ...interface{}) ([]interface{}, error)
func GetString(v interface{}, path ...interface{}) (string, error)
func SGet(m map[string]interface{}, path ...string) (interface{}, error)
func SSet(m map[string]interface{}, value interface{}, path ...string) error
func Set(v interface{}, value interface{}, path ...interface{}) error


github.com/tidwall/gjson

func AddModifier(name string, fn func(json, arg string) string)
func ForEachLine(json string, iterator func(line Result) bool)
func ModifierExists(name string, fn func(json, arg string) string) bool
func Valid(json string) bool
func ValidBytes(json []byte) bool
type Result

    func Get(json, path string) Result
    func GetBytes(json []byte, path string) Result
    func GetMany(json string, path ...string) []Result
    func GetManyBytes(json []byte, path ...string) []Result
    func Parse(json string) Result
    func ParseBytes(json []byte) Result

    func (t Result) Array() []Result
    func (t Result) Bool() bool
    func (t Result) Exists() bool
    func (t Result) Float() float64
    func (t Result) ForEach(iterator func(key, value Result) bool)
    func (t Result) Get(path string) Result
    func (t Result) Int() int64
    func (t Result) IsArray() bool
    func (t Result) IsObject() bool
    func (t Result) Less(token Result, caseSensitive bool) bool
    func (t Result) Map() map[string]Result
    func (t Result) String() string
    func (t Result) Time() time.Time
    func (t Result) Uint() uint64
    func (t Result) Value() interface{}



*/
